-- huateng.jpress_addon_recruit2 definition

CREATE TABLE `jpress_addon_recruit2` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `position_type` varchar(225) DEFAULT NULL COMMENT '职位类型',
  `release_time` datetime DEFAULT NULL COMMENT '发布时间',
  `effective_date` datetime DEFAULT NULL COMMENT '有效日期',
  `basic_requirements` varchar(225) DEFAULT NULL COMMENT '基本要求',
  `job_description` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT '职位描述',
  `working_place` varchar(225) DEFAULT NULL COMMENT '工作地点',
  `work_address` varchar(225) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '工作地址',
  `registration_method` varchar(225) DEFAULT NULL COMMENT '报名方式',
  `lcon_url` varchar(100) DEFAULT NULL COMMENT '图标',
  `language` varchar(2) NOT NULL COMMENT '语言',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
