package io.jpress.addon.recruit2.directive;

import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.template.Env;
import com.jfinal.template.io.Writer;
import com.jfinal.template.stat.Scope;
import io.jboot.web.controller.JbootControllerContext;
import io.jboot.web.directive.annotation.JFinalDirective;
import io.jboot.web.directive.base.JbootDirectiveBase;
import io.jboot.web.directive.base.PaginateDirectiveBase;
import io.jpress.JPressOptions;
import io.jpress.addon.recruit2.model.JpressAddonRecruit2;
import io.jpress.addon.recruit2.service.JpressAddonRecruit2Service;
import io.jpress.commons.directive.DirectveKit;

import javax.servlet.http.HttpServletRequest;


@JFinalDirective("Recruit2DirectiveList")
public class Recruit2Directive extends JbootDirectiveBase {

    @Inject
    private JpressAddonRecruit2Service service;

    @Override
    public void onRender(Env env, Scope scope, Writer writer) {

        Controller controller = JbootControllerContext.get();
        int page = controller.getParaToInt(1, 1);
        int pageSize = getPara("pageSize", scope,10);
        Page<JpressAddonRecruit2> paginate = service.paginate(page, pageSize);
        scope.setLocal("Recruit2DirectiveList",paginate);
        renderBody(env, scope, writer);

    }

    @JFinalDirective("Recruit2DirectivePaginate")
    public static class TemplatePaginateDirective extends PaginateDirectiveBase {

        @Override
        protected String getUrl(int pageNumber, Env env, Scope scope, Writer writer) {
            HttpServletRequest request = JbootControllerContext.get().getRequest();
            String url = request.getRequestURI();
            String contextPath = JFinal.me().getContextPath();

            boolean firstGotoIndex = getPara("firstGotoIndex", scope, false);

            if (pageNumber == 1 && firstGotoIndex) {
                return contextPath + "/";
            }

            // 如果当前页面是首页的话
            // 需要改变url的值，因为 上一页或下一页是通过当前的url解析出来的
            if (url.equals(contextPath + "/")) {
                url = contextPath + "/recruit2/index"
                        + JPressOptions.getAppUrlSuffix();
            }
            return DirectveKit.replacePageNumber(url, pageNumber);
        }

        @Override
        protected Page<?> getPage(Env env, Scope scope, Writer writer) {
            return (Page<?>) scope.get("Recruit2DirectiveList");
        }

    }

    @Override
    public boolean hasEnd() {
        return true;
    }
}
